// Copyright Aaron Terrell 2018

#pragma once

#include "OpenDoor.h"

#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Public/TimerManager.h"
#include "Components/PrimitiveComponent.h"
#include "Grabber.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	
	Owner = GetOwner();
	if (!PressurePlate)
	{
			UE_LOG(LogTemp, Error, TEXT(" %s is missing pressure plate"), *(GetOwner()->GetName()));	
	}


}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);



	// Poll the trigger volume
	if (GetTotalMassofActorsOnPlate() >= TriggerMass) // TODO make into a parameter
	{
		OnOpen.Broadcast();
	}
	else
	{
		OnClose.Broadcast();
	}

}

float UOpenDoor::GetTotalMassofActorsOnPlate()
{

	float TotalMass = 0.f;
	
	// Find all the overlapping actor 
	TArray<AActor*> OverLappingActors;
	if (!PressurePlate) { return TotalMass; }
	PressurePlate->GetOverlappingActors(OUT OverLappingActors);
	
	//Iterate though them adding their masses
	for (const auto* ActorsLoop: OverLappingActors)
	{
		TotalMass += ActorsLoop->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		UE_LOG(LogTemp, Warning, TEXT("%s on pressure plate"), *ActorsLoop->GetName());
	}

	return TotalMass;
}